import time
from lib.GameInstance import GameInstance

game = GameInstance.find_instance()

if not game:
    print('Game Not Found')
    quit()

game.activate()
game.set_mouse_pos(0.5, 0.5)

while True:
    print(game.get_mouse_pos())
    time.sleep(0.5)