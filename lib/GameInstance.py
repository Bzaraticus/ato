import math
import time
import win32api, win32gui, win32con, win32com.client
from .locations import locations
from .delays import delays

class GameInstance:
    def __init__(self, hwnd):
        self.hwnd = hwnd
        self.bounds = None
        self.last_mouse_pos = None
        self.shell = win32com.client.Dispatch('WScript.Shell')


    @staticmethod
    def find_instance():
        hwnd = win32gui.FindWindow(None, 'AcrossTheObelisk')
        
        if not hwnd:
            return None

        return GameInstance(hwnd)


    def calculate_game_rect(self):
        client_rect = win32gui.GetClientRect(self.hwnd)
        window_top_left = win32gui.ClientToScreen(self.hwnd, (client_rect[0], client_rect[1]))
        window_bottom_right = win32gui.ClientToScreen(self.hwnd, (client_rect[2], client_rect[3]))

        # The game is always 16:9
        # It adds black bars where needed.

        window_width = window_bottom_right[0] - window_top_left[0]
        window_height = window_bottom_right[1] - window_top_left[1]
        window_ratio = window_width / window_height
        target_ratio = 16 / 9
        game_top = None
        game_left = None
        game_width = None
        game_height = None

        if window_ratio > target_ratio:
            game_width = math.floor(window_height * target_ratio)
            game_height = window_height
            game_top = window_top_left[1]
            game_left = math.floor(window_top_left[0] + (window_width - game_width) / 2)
        else:
            game_width = window_width
            game_height = math.floor(window_width / target_ratio)
            game_top = math.floor(window_top_left[1] + (window_height - game_height) / 2)
            game_left = window_top_left[0]

        return (game_left, game_top, game_width, game_height)


    def activate(self):
        # Why 'activate'? it sounds cool!

        win32gui.ShowWindow(self.hwnd, win32con.SW_RESTORE)
        win32gui.SetForegroundWindow(self.hwnd)

        # TODO: For some reason the menu is aligned weirdly.
        # So force the window to a set size
        win32gui.MoveWindow(self.hwnd, 100, 100, 1600, 900, True)

        self.bounds = self.calculate_game_rect()


    def set_mouse_pos(self, percent_x, percent_y):
        pos_x = self.bounds[0] + math.floor(self.bounds[2] * percent_x)
        pos_y = self.bounds[1] + math.floor(self.bounds[3] * percent_y)

        win32api.SetCursorPos((pos_x, pos_y))

        self.last_mouse_pos = (percent_x, percent_y)


    def get_mouse_pos(self):
        pos = win32api.GetCursorPos()

        percent_x = round((pos[0] - self.bounds[0]) / self.bounds[2], 2)
        percent_y = round((pos[1] - self.bounds[1]) / self.bounds[3], 2)

        return (percent_x, percent_y)


    def has_mouse_moved(self):
        if not self.last_mouse_pos:
            return False
        
        pos = self.get_mouse_pos()
        diff_x = abs(self.last_mouse_pos[0] - pos[0])
        diff_y = abs(self.last_mouse_pos[1] - pos[1])
        eps = 0.00001

        return diff_x > eps or diff_y > eps


    def click(self):
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
        time.sleep(0.2)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)
        time.sleep(0.1)
    

    def click_pos(self, pos_x, pos_y):
        self.set_mouse_pos(pos_x, pos_y)
        time.sleep(0.1)
        self.click()


    def click_location(self, name):
        if name not in locations:
            return
        
        location = locations[name]

        self.click_pos(location[0], location[1])
    

    def keys(self, str):
        for char in str:
            vk = win32api.VkKeyScan(char)
            scan = win32api.MapVirtualKey(vk, 0)
            win32api.keybd_event(vk, scan, 0, 0)
            time.sleep(0.1)
            win32api.keybd_event(vk, scan, win32con.KEYEVENTF_KEYUP, 0)
            time.sleep(0.1)


    def named_delay(self, name):
        if name not in delays:
            return

        time.sleep(delays[name])
        

    def drag(self, pos_from, pos_to):
        self.set_mouse_pos(pos_from[0], pos_from[1])
        time.sleep(0.1)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
        time.sleep(0.1)
        self.set_mouse_pos(pos_to[0], pos_to[1])
        time.sleep(0.1)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)
        time.sleep(0.1)


    def play_card(self, card_index, target_index = -1):
        self.keys(str(card_index))

        if target_index != -1:
            self.keys(str(target_index))

        self.named_delay('combat.play_card')


    def end_turn(self):
        self.keys(' ')
        self.named_delay('combat.end_turn')

    
    def end_combat(self):
        self.named_delay('combat.end')

        # Skip rewards
        for i in range(0, 4):
            percent_y = 0.28 + i * 0.19
            self.click_pos(0.79, percent_y)

        self.named_delay('combat.rewards_claimed')


    def click_book_option(self, num):
        percent_y = 0.18 + (num - 1) * 0.11
        
        self.click_pos(0.75, percent_y)
        self.named_delay('book.option')

    
    def click_book_continue(self):
        self.click_location('book.continue')
        self.named_delay('book.continue')


    def new_game(self):
        self.click_location('menu.play_button')
        self.named_delay('menu.new_game')
        self.click_location('menu.adventure_mode')
        self.click_location('menu.delete_save_3')
        self.click_location('menu.confirm_delete_save')
        self.click_location('menu.save_slot_3')
        self.named_delay('menu.select_save_slot')

    
    def setup_team(self, characters):
        team_slots_y = 0.39
        team_slots_x = [0.57, 0.65, 0.73, 0.81]
        team_clear_pos = (0.7, 0.6)

        # Clear existing team, if any
        for i in range(0, 4):
            slot_pos = (team_slots_x[i], team_slots_y)
            self.drag(slot_pos, team_clear_pos)

        # Build new team
        for i in range(0, 4):
            location_name = f'menu.character.{characters[i]}'
            slot_pos = (team_slots_x[i], team_slots_y)
            self.drag(locations[location_name], slot_pos)

    
    def enter_seed(self, seed):
        self.click_location('menu.enter_seed')
        self.keys(seed)
        self.click_location('menu.accept_seed')


    def start_game(self):
        self.click_location('menu.begin_adventure')
        self.named_delay('menu.load_intro')
        self.click_location('menu.skip_intro')
        self.named_delay('menu.load_game')

    
    def navigate(self, name):
        self.click_location(f'map.{name}')
        self.named_delay('map.navigate')


    def exit_run(self):
        self.click_location('menu.exit_run')
        self.click_location('menu.confirm_exit_run')
        self.named_delay('menu.exit_run')

    
    def overcharge(self, num):
        for i in range(0, num):
            self.click_location('overcharge.increase')

        self.click_location('overcharge.confirm')