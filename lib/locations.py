# Named locations (could be map nodes, exit button etc)

locations = {
    'menu.play_button': (0.05, 0.48),
    'menu.adventure_mode': (0.24, 0.45),
    'menu.save_slot_3': (0.84, 0.8),
    'menu.delete_save_3': (0.95, 0.72),
    'menu.confirm_delete_save': (0.6, 0.6),
    'menu.character.navalea': (0.26, 0.88),
    'menu.character.zek': (0.36, 0.52),
    'menu.character.malukah': (0.26, 0.69),
    'menu.character.nezglekt': (0.36, 0.69),
    'menu.enter_seed': (0.93, 0.93),
    'menu.accept_seed': (0.6, 0.51),
    'menu.begin_adventure': (0.7, 0.83),
    'menu.skip_intro': (0.93, 0.93),
    'menu.exit_run': (0.98, 0.02),
    'menu.confirm_exit_run': (0.56, 0.56),
    
    'town.ready': (0.92, 0.92),

    'book.continue': (0.86, 0.88),

    'map.town': (0.14, 0.55),
    'map.caravan': (0.2, 0.44),
    'map.the_last_sentinel': (0.6, 0.44),
    'map.story_time': (0.71, 0.42),
    'map.the_failed_expedition': (0.81, 0.48),
    'map.old_tree': (0.89, 0.43),

    'item_rewards.gold': (0.82, 0.68),

    'overcharge.increase': (0.55, 0.48),
    'overcharge.confirm': (0.49, 0.66),
}