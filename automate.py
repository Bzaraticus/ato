import re
import time
from lib.GameInstance import GameInstance

# Functions

def exec_command(cmd, args):
    game.activate()

    if cmd == 'new':
        game.new_game()
    elif cmd == 'team':
        game.setup_team(args[0:4])
    elif cmd == 'seed':
        game.enter_seed(args[0])
    elif cmd == 'start':
        game.start_game()
    elif cmd == 'exit':
        game.exit_run()
    elif cmd == 'click':
        game.click_location(args[0])
    elif cmd == 'delay':
        if args[0].isnumeric():
            time.sleep(float(args[0]))
        else:
            game.named_delay(args[0])
    elif cmd == 'navigate':
        game.navigate(args[0])
    elif cmd == 'book':
        if args[0] == 'continue':
            game.click_book_continue()
        else:
            game.click_book_option(int(args[0]))
    elif cmd == 'play':
        card_index = int(args[0])
        target_index = -1

        if len(args) > 1:
            target_index = int(args[1])

        game.play_card(card_index, target_index)
    elif cmd == 'endturn':
        game.end_turn()
    elif cmd == 'overcharge':
        game.overcharge(int(args[0]))
    elif cmd == 'endcombat':
        game.end_combat()


def parse_command(str):
    args = re.split('\s+', str.strip())    
    cmd = args.pop(0).lower()
    return (cmd, args)

# Init

game = GameInstance.find_instance()

if not game:
    print('Game Not Found')
    quit()

fp = open('script.txt', 'r')
lines = fp.readlines()
fp.close()

game.activate()
game.set_mouse_pos(0.5, 0.5)

# Main loop

run_loop = True

while run_loop:
    for line in lines:
        cmd, args = parse_command(line)

        if not len(cmd) or cmd[0] == '#':
            continue

        print(line.strip())
        exec_command(cmd, args)

        if game.has_mouse_moved():
            run_loop = False
            break